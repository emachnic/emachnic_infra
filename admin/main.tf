provider "aws" {
  profile = "emachnic-admin" 
  region  = "us-east-1"
}

resource "aws_iam_group" "deployers"  {
  name = "deployers"
}

resource "aws_iam_group_policy_attachment" "deployers_policy_poweruser_attachment" {
  group      = aws_iam_group.deployers.name
  policy_arn = "arn:aws:iam::aws:policy/PowerUserAccess"
}

resource "aws_iam_group_policy_attachment" "deployers_policy_iam_readonly_attachment" {
  group      = aws_iam_group.deployers.name
  policy_arn = "arn:aws:iam::aws:policy/IAMReadOnlyAccess"
}

resource "aws_iam_user" "deploy" {
  name = "deploy"
}

resource "aws_iam_user_group_membership" "deploy" {
  user = aws_iam_user.deploy.name

  groups = [
    aws_iam_group.deployers.name
  ]
}

resource "aws_iam_user_ssh_key" "deploy_emachnic" {
  username   = aws_iam_user.deploy.name
  encoding   = "SSH"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA0zmqgbse4uET3JjThlhoBcEYr0VY9wGAelAmztLBvYQ3P/hsUVka6rsMllSKTvLTqqJUk7krY8CqSma6/6DubmHMqCodnHrKXcjmdRvuPAB6eBLhSbJAkatKHcOQ3ujZmg2U+XcSPdcAZl5dp1fq9Nh1L5hcsfQ9uPgdfF6dgsgJhkzoAhJlghICE+HLAXQV7e8w/o/J+cHRdiBpGvBZuguRaYo2eqCnBihYKh/Xv4Pbh/YEHvg6IXg6NFi4Vb34EpYrqr7BNsMU0OLyOgU/pccuB2sGnHN1hT1ID66aDMBTYfpMlaCg/FerUP1BR1dwSW0vdfo1Oe9MHe1hnXXFGQ== emachnic@broadmac.net"
}
