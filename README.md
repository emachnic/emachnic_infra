# Evan Machnic Infrastructure

This repo contains [Terraform](https://terraform.io) code for deploying my
personal infrastructure.

## Folder Structure

### `admin` folder

The `admin` folder is used for configuring the administrative part of the
cloud, like IAM users and groups. The user that runs this code in this
folder needs to have adiminstrative permissions but should not be the
root user.

### `infra` folder

The `infra` folder is used for setting up the infrastructure. The user
running the code in this folder can have more relaxed permissions but
I've set it up to have power user permissions.

